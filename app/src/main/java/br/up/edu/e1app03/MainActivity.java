package br.up.edu.e1app03;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void somar(View v){

        EditText caixa1 = (EditText) findViewById(R.id.txtNumero1);
        EditText caixa2 = (EditText) findViewById(R.id.txtNumero2);
        EditText caixa3 = (EditText) findViewById(R.id.txtResultado);

        String txtDaCaixa1 = caixa1.getText().toString();
        String txtDaCaixa2 = caixa2.getText().toString();

        int vlr1 = Integer.parseInt(txtDaCaixa1);
        int vlr2 = Integer.parseInt(txtDaCaixa2);

        int resultado = vlr1 + vlr2;

        caixa3.setText(String.valueOf(resultado));
    }

    public void subtrair(View v){

        EditText caixa1 = (EditText) findViewById(R.id.txtNumero1);
        EditText caixa2 = (EditText) findViewById(R.id.txtNumero2);
        EditText caixa3 = (EditText) findViewById(R.id.txtResultado);

        String txtDaCaixa1 = caixa1.getText().toString();
        String txtDaCaixa2 = caixa2.getText().toString();

        int vlr1 = Integer.parseInt(txtDaCaixa1);
        int vlr2 = Integer.parseInt(txtDaCaixa2);

        int resultado = vlr1 - vlr2;

        caixa3.setText(String.valueOf(resultado));
    }

    public void multiplicar(View v){

        EditText caixa1 = (EditText) findViewById(R.id.txtNumero1);
        EditText caixa2 = (EditText) findViewById(R.id.txtNumero2);
        EditText caixa3 = (EditText) findViewById(R.id.txtResultado);

        String txtDaCaixa1 = caixa1.getText().toString();
        String txtDaCaixa2 = caixa2.getText().toString();

        int vlr1 = Integer.parseInt(txtDaCaixa1);
        int vlr2 = Integer.parseInt(txtDaCaixa2);

        int resultado = vlr1 * vlr2;

        caixa3.setText(String.valueOf(resultado));
    }


    public void dividir(View v){

        EditText caixa1 = (EditText) findViewById(R.id.txtNumero1);
        EditText caixa2 = (EditText) findViewById(R.id.txtNumero2);
        EditText caixa3 = (EditText) findViewById(R.id.txtResultado);

        String txtDaCaixa1 = caixa1.getText().toString();
        String txtDaCaixa2 = caixa2.getText().toString();

        int vlr1 = Integer.parseInt(txtDaCaixa1);
        int vlr2 = Integer.parseInt(txtDaCaixa2);

        int resultado = vlr1 / vlr2;

        caixa3.setText(String.valueOf(resultado));
    }

}






